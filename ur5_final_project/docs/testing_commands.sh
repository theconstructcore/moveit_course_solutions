# Test URDF XACRO
roslaunch ur5_final_project ur5_robot_urdf.launch
# Test Moveit Package not connected
roslaunch ur5_final_project_moveit demo.launch
# Test Moveit with Controllers activated for Gazebo
roslaunch ur5_final_project_moveit ur5_final_project_planning_execution.launch
# Normal error: [ERROR] [1587460464.898133249, 2359.480000000]: Could not find the planner configuration 'None' on the param server
# Normal Error: No sensor plugin specified for octomap updater 0; ignoring.