#! /usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import tf

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)

robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()    
group = moveit_commander.MoveGroupCommander("arm")
display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=1)

pose_target = geometry_msgs.msg.Pose()
pose_target.orientation.w = 1.0
pose_target.position.x = 0.3
pose_target.position.y = 0
pose_target.position.z = 1.1
group.set_pose_target(pose_target)

rospy.loginfo("Planning Trajectory to Pose 1")
plan1 = group.plan()
rospy.loginfo("DONE Planning Trajectory to Pose 1")
rospy.loginfo("Executing Trajectory to Pose 1")
#group.go(wait=True)
rospy.loginfo("DONE Executing Trajectory to Pose 1")

#rospy.sleep(5)

pose_target = geometry_msgs.msg.Pose()
roll=0.0
pitch=0.7
yaw=0.0
quaternion = tf.transformations.quaternion_from_euler(roll, pitch, yaw)
pose_target.orientation.x = quaternion[0]
pose_target.orientation.y = quaternion[1]
pose_target.orientation.z = quaternion[2]
pose_target.orientation.w = quaternion[3]
pose_target.position.x = 0.3
pose_target.position.y = 0
pose_target.position.z = 1.2
group.set_pose_target(pose_target)

rospy.loginfo("Planning Trajectory to Pose 2")
plan1 = group.plan()
rospy.loginfo("DONE Planning Trajectory to Pose 2")
rospy.loginfo("Executing Trajectory to Pose 2")
#group.go(wait=True)
rospy.loginfo("DONE Executing Trajectory to Pose 2")


pose_target = geometry_msgs.msg.Pose()
roll=0.0
pitch=0.7
yaw=0.0
quaternion = tf.transformations.quaternion_from_euler(roll, pitch, yaw)
pose_target.orientation.x = quaternion[0]
pose_target.orientation.y = quaternion[1]
pose_target.orientation.z = quaternion[2]
pose_target.orientation.w = quaternion[3]
pose_target.position.x = 0.3
pose_target.position.y = 0
pose_target.position.z = 0.9
group.set_pose_target(pose_target)

rospy.loginfo("Planning Trajectory to Pose 2")
plan1 = group.plan()
rospy.loginfo("DONE Planning Trajectory to Pose 2")
rospy.loginfo("Executing Trajectory to Pose 2")
#group.go(wait=True)
rospy.loginfo("DONE Executing Trajectory to Pose 2")



moveit_commander.roscpp_shutdown()